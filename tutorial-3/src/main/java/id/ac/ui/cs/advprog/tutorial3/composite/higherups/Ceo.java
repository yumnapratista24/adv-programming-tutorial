package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {

    private String name;
    private double salary;

    public Ceo(String name, double salary) {
        //TODO Implement
        this.name = name;
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return "CEO";
    }
}
