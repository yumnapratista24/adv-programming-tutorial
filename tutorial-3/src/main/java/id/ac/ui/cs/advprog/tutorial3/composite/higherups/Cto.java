package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {

    private String name;
    private double salary;

    public Cto(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return "CTO";
    }
}
