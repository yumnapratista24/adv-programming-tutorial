package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees{

    private String name;
    private double salary;

    public UiUxDesigner(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return "UI/UX Designer";
    }
}
