package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class OnionTest {

    private Onion onion;

    @Before
    public void setUp() throws Exception {
        onion = new Onion();
    }

    @Test
    public void test_toString_method() {
        assertEquals(onion.toString(),"Onion");
    }

}