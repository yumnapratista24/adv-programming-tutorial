package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class BlackOlivesTest {
        private BlackOlives blackOlives;

        @Before
        public void setUp() throws Exception {
            blackOlives = new BlackOlives();
        }

        @Test
        public void test_toString_method() {
            assertEquals(blackOlives.toString(),"Black Olives");
        }
}