package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.SinglesCheese;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class FrozenClamsTest {
    private FrozenClams frozenClams;

    @Before
    public void setUp() throws Exception {
        frozenClams = new FrozenClams();
    }

    @Test
    public void test_toString_method() {
        assertEquals(frozenClams.toString(),"Frozen Clams from Chesapeake Bay");
    }


}