package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class MarinaraSauceTest {

    private MarinaraSauce marinaraSauce;

    @Before
    public void setUp() throws Exception {
        marinaraSauce = new MarinaraSauce();
    }

    @Test
    public void test_toString_method() {
        assertEquals(marinaraSauce.toString(),"Marinara Sauce");
    }

}