package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class RedPepperTest {

    private RedPepper redPepper;

    @Before
    public void setUp() throws Exception {
        redPepper = new RedPepper();
    }

    @Test
    public void test_toString_method() {
        assertEquals(redPepper.toString(),"Red Pepper");
    }


}