package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class SinglesCheeseTest {
    private SinglesCheese singlesCheese;

    @Before
    public void setUp() throws Exception {
        singlesCheese = new SinglesCheese();
    }

    @Test
    public void test_toString_method() {
        assertEquals(singlesCheese.toString(),"Singles Cheese");
    }

}