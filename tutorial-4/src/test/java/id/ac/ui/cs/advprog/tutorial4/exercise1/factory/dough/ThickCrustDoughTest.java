package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class ThickCrustDoughTest {

    private ThickCrustDough thickCrustDough;

    @Before
    public void setUp() throws Exception {
        thickCrustDough = new ThickCrustDough();
    }

    @Test
    public void test_toString_method() {
        assertEquals(thickCrustDough.toString(),"ThickCrust style extra thick crust dough");
    }

}