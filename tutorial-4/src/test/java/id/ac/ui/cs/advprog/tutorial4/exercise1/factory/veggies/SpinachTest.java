package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class SpinachTest {

    private Spinach spinach;

    @Before
    public void setUp() throws Exception {
        spinach = new Spinach();
    }

    @Test
    public void test_toString_method() {
        assertEquals(spinach.toString(),"Spinach");
    }

}