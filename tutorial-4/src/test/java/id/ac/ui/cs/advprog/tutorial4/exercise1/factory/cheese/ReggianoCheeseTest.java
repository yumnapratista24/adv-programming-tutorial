package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReggianoCheeseTest {
    private ReggianoCheese reggianoCheese;

    @Before
    public void setUp() throws Exception {
        reggianoCheese = new ReggianoCheese();
    }

    @Test
    public void test_toString_method() {
        assertEquals(reggianoCheese.toString(),"Reggiano Cheese");
    }

}