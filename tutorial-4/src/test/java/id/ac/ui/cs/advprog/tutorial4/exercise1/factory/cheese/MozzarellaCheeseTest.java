package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class MozzarellaCheeseTest {
    private MozzarellaCheese mozzarellaCheese;

    @Before
    public void setUp() throws Exception {
        mozzarellaCheese = new MozzarellaCheese();
    }

    @Test
    public void test_toString_method() {
        assertEquals(mozzarellaCheese.toString(),"Shredded Mozzarella");
    }
}