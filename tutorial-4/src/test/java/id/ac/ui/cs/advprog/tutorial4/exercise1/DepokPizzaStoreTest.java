package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepokPizzaStoreTest {

    private DepokPizzaStore depokPizzaStore;

    @Before
    public void setUp() throws Exception {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void depokPizza_test_create_Pizza() {
        Pizza pizza;

        pizza = depokPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(),"Depok Style Cheese Pizza");

        pizza = depokPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(),"Depok Style Clam Pizza");

        pizza = depokPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(),"Depok Style Veggie Pizza");
    }
}