package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class DryClamsTest {

    private DryClams dryClams;

    @Before
    public void setUp() throws Exception {
        dryClams = new DryClams();
    }

    @Test
    public void test_toString_method() {
        assertEquals(dryClams.toString(),"Dry Clams");
    }
}