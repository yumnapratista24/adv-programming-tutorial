package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class NewYorkPizzaStoreTest {

    private NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() throws Exception {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void newYorkPizza_test_create_Pizza() {
        Pizza pizza;

        pizza = newYorkPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(),"New York Style Cheese Pizza");

        pizza = newYorkPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(),"New York Style Clam Pizza");

        pizza = newYorkPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(),"New York Style Veggie Pizza");
    }

}