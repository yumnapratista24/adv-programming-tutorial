package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class FreshClamsTest {

    private FreshClams freshClams;

    @Before
    public void setUp() throws Exception {
        freshClams = new FreshClams();
    }

    @Test
    public void test_toString_method() {
        assertEquals(freshClams.toString(),"Fresh Clams from Long Island Sound");
    }

}