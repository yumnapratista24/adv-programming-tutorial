package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class MushroomTest {

    private Mushroom mushroom;

    @Before
    public void setUp() throws Exception {
        mushroom = new Mushroom();
    }

    @Test
    public void test_toString_method() {
        assertEquals(mushroom.toString(),"Mushrooms");
    }

}