package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class GarlicTest {

    private Garlic garlic;

    @Before
    public void setUp() throws Exception {
        garlic = new Garlic();
    }

    @Test
    public void test_toString_method() {
        assertEquals(garlic.toString(),"Garlic");
    }

}