package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class BlackPepperSauceTest {

    private BlackPepperSauce blackPepperSauce;

    @Before
    public void setUp() throws Exception {
        blackPepperSauce = new BlackPepperSauce();
    }

    @Test
    public void test_toString_method() {
        assertEquals(blackPepperSauce.toString(),"Black Pepper Sauce");
    }

}