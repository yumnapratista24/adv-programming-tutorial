package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlumTomatoSauceTest {

    private PlumTomatoSauce plumTomatoSauce;

    @Before
    public void setUp() throws Exception {
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    public void test_toString_method() {
        assertEquals(plumTomatoSauce.toString(),"Tomato sauce with plum tomatoes");
    }


}