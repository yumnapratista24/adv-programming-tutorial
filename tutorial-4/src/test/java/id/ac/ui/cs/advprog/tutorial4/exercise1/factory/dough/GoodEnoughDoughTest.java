package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.SinglesCheese;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class GoodEnoughDoughTest {

    private GoodEnoughDough goodEnoughDough;

    @Before
    public void setUp() throws Exception {
        goodEnoughDough = new GoodEnoughDough();
    }

    @Test
    public void test_toString_method() {
        assertEquals(goodEnoughDough.toString(),"Good Enough Dough");
    }
}