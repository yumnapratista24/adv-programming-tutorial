package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParmesanCheeseTest {
    private ParmesanCheese parmesanCheese;

    @Before
    public void setUp() throws Exception {
        parmesanCheese = new ParmesanCheese();
    }

    @Test
    public void test_toString_method() {
        assertEquals(parmesanCheese.toString(),"Shredded Parmesan");
    }
}