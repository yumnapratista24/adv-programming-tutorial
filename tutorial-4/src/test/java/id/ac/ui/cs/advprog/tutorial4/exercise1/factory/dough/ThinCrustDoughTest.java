package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class ThinCrustDoughTest {

    private ThinCrustDough thinCrustDough;

    @Before
    public void setUp() throws Exception {
        thinCrustDough = new ThinCrustDough();
    }

    @Test
    public void test_toString_method() {
        assertEquals(thinCrustDough.toString(),"Thin Crust Dough");
    }
}