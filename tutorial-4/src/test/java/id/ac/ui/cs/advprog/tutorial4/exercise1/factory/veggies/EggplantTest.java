package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class EggplantTest {
    private Eggplant eggplant;

    @Before
    public void setUp() throws Exception {
        eggplant = new Eggplant();
    }

    @Test
    public void test_toString_method() {
        assertEquals(eggplant.toString(),"Eggplant");
    }
}