package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class SinglesCheese implements Cheese {

    public String toString() {
        return "Singles Cheese";
    }
}
