package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class GoodEnoughDough implements Dough{

    public String toString() {
        return "Good Enough Dough";
    }
}
