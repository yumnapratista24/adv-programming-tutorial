package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class DryClams implements Clams{

    public String toString() {
        return "Dry Clams";
    }
}
