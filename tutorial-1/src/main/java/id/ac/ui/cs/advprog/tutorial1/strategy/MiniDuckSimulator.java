package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MiniDuckSimulator {

    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        Squeak squeak = new Squeak();
        FlyNoWay flyNoWay = new FlyNoWay();
        mallard.setQuackBehavior(squeak);
        mallard.setFlyBehavior(flyNoWay);
        mallard.performQuack();
        mallard.performFly();

//         TODO: Fix me!
        Duck model = new ModelDuck();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }


}
